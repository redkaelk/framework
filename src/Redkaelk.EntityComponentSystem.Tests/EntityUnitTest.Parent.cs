﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Redkaelk.EntityComponentSystem;

namespace Redkaelk.UnitTests.EntityComponentSystem
{
    public partial class EntityUnitTest
    {
        [TestMethod]
        public void Test_Parent_SetParentForTheFirstTime()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager)
            {
                Parent = parent
            };

            Assert.AreEqual(parent, child.Parent);
            Assert.IsTrue(parent.Children.Contains(child));
        }

        [TestMethod]
        public void Test_Parent_SetParentToNullAfterHavingBeenAssignedToNotNull()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager)
            {
                Parent = parent
            };
            child.Parent = null;

            Assert.IsNull(child.Parent);
            Assert.IsFalse(parent.Children.Contains(child));
        }

        [TestMethod]
        public void Test_Parent_SetParentToNullAfterHavingBeenAssignedToNull()
        {
            var manager = Substitute.For<IManager>();
            var child = new Entity(manager)
            {
                Parent = null
            };

            Assert.IsNull(child.Parent);
        }

        [TestMethod]
        public void Test_Parent_SetTheSameParentTwice()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager)
            {
                Parent = parent
            };
            child.Parent = parent;

            Assert.AreEqual(parent, child.Parent);
            Assert.IsTrue(parent.Children.Contains(child));
        }

        [TestMethod]
        public void Test_Parent_ChangeParent()
        {
            var manager = Substitute.For<IManager>();
            var parent1 = new Entity(manager);
            var parent2 = new Entity(manager);
            var child = new Entity(manager)
            {
                Parent = parent1
            };
            child.Parent = parent2;

            Assert.AreEqual(parent2, child.Parent);
            Assert.IsFalse(parent1.Children.Contains(child));
            Assert.IsTrue(parent2.Children.Contains(child));
        }
    }
}
