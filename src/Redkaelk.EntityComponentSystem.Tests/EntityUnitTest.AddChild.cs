﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Redkaelk.EntityComponentSystem;

namespace Redkaelk.UnitTests.EntityComponentSystem
{
    public partial class EntityUnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_AddChild_ChildIsNull()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            parent.AddChild(null);
        }

        [TestMethod]
        public void Test_AddChild_ChildDoesNotHaveAParent()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager);
            parent.AddChild(child);

            Assert.IsTrue(parent.Children.Contains(child));
            Assert.IsFalse(parent.Children.Contains(parent));
            Assert.IsFalse(child.Children.Contains(child));
            Assert.IsFalse(child.Children.Contains(parent));
            Assert.AreEqual(parent, child.Parent);
            Assert.IsNull(parent.Parent);
        }

        [TestMethod]
        public void Test_AddChild_ChangeParent()
        {
            var manager = Substitute.For<IManager>();
            var parent1 = new Entity(manager);
            var parent2 = new Entity(manager);
            var child = new Entity(manager);
            parent1.AddChild(child);
            parent2.AddChild(child);

            Assert.IsTrue(parent2.Children.Contains(child));
            Assert.IsFalse(parent2.Children.Contains(parent2));
            Assert.IsFalse(child.Children.Contains(parent2));
            Assert.AreEqual(parent2, child.Parent);
            Assert.IsNull(parent2.Parent);

            Assert.IsFalse(parent1.Children.Contains(child));
            Assert.IsFalse(parent1.Children.Contains(parent1));
            Assert.IsFalse(child.Children.Contains(parent1));
            Assert.IsNull(parent1.Parent);

            Assert.IsFalse(child.Children.Contains(child));
        }

        [TestMethod]
        public void Test_AddChild_AddTwice()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager);
            parent.AddChild(child);
            parent.AddChild(child);

            Assert.IsTrue(parent.Children.Contains(child));
            Assert.IsFalse(parent.Children.Contains(parent));
            Assert.IsFalse(child.Children.Contains(parent));
            Assert.IsNull(parent.Parent);

            Assert.IsFalse(child.Children.Contains(child));
        }

        [TestMethod]
        public void Test_AddChild_OnlyGetsAddedOnce()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager);
            parent.AddChild(child);

            Assert.IsTrue(parent.Children.Contains(child));
            Assert.IsFalse(parent.Children.Contains(parent));
            Assert.IsFalse(child.Children.Contains(parent));
            Assert.IsFalse(child.Children.Contains(child));
            Assert.IsNull(parent.Parent);
            Assert.AreEqual(1, parent.Children.Count());
        }
    }
}
