﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Redkaelk.EntityComponentSystem;

namespace Redkaelk.UnitTests.EntityComponentSystem
{
    public partial class EntityUnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_RemoveChild_ChildIsNull()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            parent.RemoveChild(null);
        }

        [TestMethod]
        public void Test_RemoveChild_BelongsToTheParent()
        {
            var manager = Substitute.For<IManager>();
            var parent = new Entity(manager);
            var child = new Entity(manager);
            parent.AddChild(child);
            parent.RemoveChild(child);

            Assert.IsNull(child.Parent);
            Assert.IsNull(parent.Parent);
            Assert.IsFalse(child.Children.Contains(child));
            Assert.IsFalse(child.Children.Contains(parent));
            Assert.IsFalse(parent.Children.Contains(child));
            Assert.IsFalse(parent.Children.Contains(parent));
        }

        [TestMethod]
        public void Test_RemoveChild_DoesNotBelongToTheParent()
        {
            var manager = Substitute.For<IManager>();
            var parent1 = new Entity(manager);
            var parent2 = new Entity(manager);
            var child = new Entity(manager);
            parent1.AddChild(child);
            parent2.RemoveChild(child);

            Assert.AreEqual(child.Parent, parent1);
            Assert.IsNull(parent1.Parent);
            Assert.IsNull(parent2.Parent);
            Assert.IsFalse(child.Children.Contains(child));
            Assert.IsFalse(child.Children.Contains(parent1));
            Assert.IsFalse(child.Children.Contains(parent2));
            Assert.IsTrue(parent1.Children.Contains(child));
            Assert.IsFalse(parent1.Children.Contains(parent1));
            Assert.IsFalse(parent2.Children.Contains(child));
            Assert.IsFalse(parent2.Children.Contains(parent2));
        }
    }
}
