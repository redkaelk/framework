using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.UnitTests
{
    public partial class EnumerableExtensionsTests
    {
        [TestMethod]
        public void Test_ForEach_FailsIfActionThrowsException()
        {
            // arrange
            var hitCount = 0;
            var array = new int[10];
            void action(int i)
            {
                hitCount++;
                throw new ArgumentException();
            }

            // act
            try
            {
                EnumerableExtensions.ForEach(array, action);
            }
            catch (ArgumentException)
            {
                // expected
            }
            catch
            {
                Assert.Fail();
            }

            // assert
            Assert.AreEqual(1, hitCount);
        }

        [TestMethod]
        public void Test_ForEach_IteratesOverAllElementsInArray()
        {
            // arrange
            var hitCount = 0;
            var array = new int[10];
            void action(int i)
            {
                hitCount++;
            }

            // act
            EnumerableExtensions.ForEach(array, action);

            // assert
            Assert.AreEqual(array.Length, hitCount);
        }
    }
}
