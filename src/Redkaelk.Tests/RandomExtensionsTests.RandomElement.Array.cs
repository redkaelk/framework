﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_RandomElement_EmptyStringArray()
        {
            var random = new Random();
            var array = Array.Empty<string>();
            var element = random.RandomElement(array);
            Assert.IsNull(element);
        }

        [TestMethod]
        public void Test_RandomElement_EmptyLongArray()
        {
            var random = new Random();
            var array = Array.Empty<long>();
            var element = random.RandomElement(array);
            Assert.AreEqual(0L, element);
        }

        [TestMethod]
        public void Test_RandomElement_StringArray()
        {
            var random = new Random();
            var array = new string[]
            {
                "bird",
                "dog",
                "cat",
            };
            var element = random.RandomElement(array);
            var index = Array.IndexOf(array, element);
            Assert.IsTrue(index > -1);
        }

        [TestMethod]
        public void Test_RandomElement_LongArray()
        {
            var random = new Random();
            var array = new long[]
            {
                1L,
                2L,
                3L,
            };
            var element = random.RandomElement(array);
            var index = Array.IndexOf(array, element);
            Assert.IsTrue(index > -1);
        }
    }
}
