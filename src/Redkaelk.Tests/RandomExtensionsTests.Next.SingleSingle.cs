﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redkaelk.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_Next_NeverReturnMaxOrGreater()
        {
            var random = new Random();
            for (var index = 0; index < 100000; index++)
            {
                var actual = random.Next(0.0f, 0.1f);
                Assert.IsTrue(actual < 0.1f);
            }
        }

        [TestMethod]
        public void Test_Next_NeverReturnLessThanMin()
        {
            var random = new Random();
            for (var index = 0; index < 100000; index++)
            {
                var actual = random.Next(0.0f, 0.1f);
                Assert.IsTrue(actual >= 0.0f);
            }
        }

        [TestMethod]
        public void Test_Next_AlrightDistribution()
        {
            const int count = 100000;
            const float threshold = count * 0.51f;

            var list = new List<float>(count);
            var random = new Random();
            for (var index = 0; index < count; index++)
            {
                var actual = random.Next(0.0f, 100.0f);
                list.Add(actual);
            }

            var below = list.Count(n => n < 50.0f);
            var above = list.Count - below;
            Assert.IsTrue(below < threshold);
            Assert.IsTrue(above < threshold);
        }
    }
}
