using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Redkaelk.UnitTests
{
    public partial class EnumerableExtensionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_MinOrDefault_ThrowsExceptionIfSourceIsNull()
        {
            EnumerableExtensions.MinOrDefault(source: null, comparer: Comparer<int>.Default, defaultValue: 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_MinOrDefault_ThrowsExceptionIfComparerIsNull()
        {
            EnumerableExtensions.MinOrDefault(source: Array.Empty<int>(), comparer: null, defaultValue: 0);
        }

        [TestMethod]
        public void Test_MinOrDefault_EmptySourceReturnsDefaultValue()
        {
            var actual = EnumerableExtensions.MinOrDefault(source: Array.Empty<int>(), defaultValue: 42);
            Assert.AreEqual(42, actual);
        }

        [TestMethod]
        public void Test_MinOrDefault_SourceWithSingleElementReturnsMaxValue()
        {
            var actual = EnumerableExtensions.MinOrDefault(source: new int[] { 1 }, defaultValue: 0);
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void Test_MinOrDefault_NonEmptySourceReturnsMaxValue()
        {
            var actual = EnumerableExtensions.MinOrDefault(source: new int[] { 1, 2 }, defaultValue: 0);
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void Test_MinOrDefault_NonEmptySourceAndReverseComparerReturnsMinValue()
        {
            var actual = EnumerableExtensions.MinOrDefault(source: new int[] { 1, 2 }, comparer: new ReverseComparer(), defaultValue: 0);
            Assert.AreEqual(2, actual);
        }
    }
}
