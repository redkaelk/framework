using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.UnitTests
{
    public partial class EnumerableExtensionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_ContainsAll_ThrowsExceptionIfSourceIsNull()
        {
            EnumerableExtensions.ContainsAll(source: null, elements: Array.Empty<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_ContainsAll_ThrowsExceptionIfElementsIsNull()
        {
            EnumerableExtensions.ContainsAll(source: Array.Empty<string>(), elements: null);
        }

        [TestMethod]
        public void Test_ContainsAll_DoesNotThrowExceptionIfComparerIsNull()
        {
            EnumerableExtensions.ContainsAll(source: Array.Empty<string>(), comparer: null, elements: Array.Empty<string>());
        }

        [TestMethod]
        public void Test_ContainsAll_EmptySourceAndEmptyElementsReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: Array.Empty<string>(), elements: Array.Empty<string>());
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_NonEmptySourceAndEmptyElementsReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "bob" }, elements: Array.Empty<string>());
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_EmptySourceAndNonEmptyElementsReturnsFalse()
        {
            var actual = EnumerableExtensions.ContainsAll(source: Array.Empty<string>(), elements: new string[] { "bob" });
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_NonEmptySourceAndNonEmptyElementsReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "bob" }, elements: new string[] { "bob" });
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_SourceDoesNotContainAnyOfTheElementsReturnsFalse()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "bob" }, elements: new string[] { "cat", "dog" });
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_SourceOnlyContainsOneOfTheElementsReturnsFalse()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "bob" }, elements: new string[] { "cat", "dog", "bob" });
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_SourceOnlyContainsTwoOfTheElementsReturnsFalse()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "bob", "cat" }, elements: new string[] { "cat", "dog", "bob" });
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_SourceContainsAllOfTheElementsInRandomOrderReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "dog", "bob", "cat" }, elements: new string[] { "cat", "dog", "bob" });
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_SourceContainsAllElementsAndAdditionalItemsReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "dog", "bob", "cat", "elephant" }, elements: new string[] { "cat", "dog", "bob" });
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ContainsAll_ElementsContainsDuplicateAndSourceHasSingleInstanceReturnsTrue()
        {
            var actual = EnumerableExtensions.ContainsAll(source: new string[] { "dog", "cat" }, elements: new string[] { "dog", "dog" });
            Assert.IsTrue(actual);
        }
    }
}
