using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Redkaelk.UnitTests
{
    public partial class EnumerableExtensionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_MaxOrDefault_ThrowsExceptionIfSourceIsNull()
        {
            EnumerableExtensions.MaxOrDefault(source: null, comparer: Comparer<int>.Default, defaultValue: 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_MaxOrDefault_ThrowsExceptionIfComparerIsNull()
        {
            EnumerableExtensions.MaxOrDefault(source: Array.Empty<int>(), comparer: null, defaultValue: 0);
        }

        [TestMethod]
        public void Test_MaxOrDefault_EmptySourceReturnsDefaultValue()
        {
            var actual = EnumerableExtensions.MaxOrDefault(source: Array.Empty<int>(), defaultValue: 42);
            Assert.AreEqual(42, actual);
        }

        [TestMethod]
        public void Test_MaxOrDefault_SourceWithSingleElementReturnsMaxValue()
        {
            var actual = EnumerableExtensions.MaxOrDefault(source: new int[] { 1 }, defaultValue: 0);
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void Test_MaxOrDefault_NonEmptySourceReturnsMaxValue()
        {
            var actual = EnumerableExtensions.MaxOrDefault(source: new int[] { 1, 2 }, defaultValue: 0);
            Assert.AreEqual(2, actual);
        }

        [TestMethod]
        public void Test_MaxOrDefault_NonEmptySourceAndReverseComparerReturnsMinValue()
        {
            var actual = EnumerableExtensions.MaxOrDefault(source: new int[] { 1, 2 }, comparer: new ReverseComparer(), defaultValue: 0);
            Assert.AreEqual(1, actual);
        }

        private class ReverseComparer : Comparer<int>
        {
            public override int Compare(int x, int y)
            {
                if (x > y)
                {
                    return -1;
                }

                if (x < y)
                {
                    return 1;
                }

                return 0;
            }
        }
    }
}
