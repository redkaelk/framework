using System;
using System.Runtime.InteropServices;

namespace Redkaelk.Serialization.UnitTests
{
    public partial class MarshalStructSerializerTests
    {
        private struct StructStub
        {
            public byte Byte;
            public int Int;
            public StringSplitOptions Enum;
            public DateTime DateTime;
        }

        private struct LPStrStructStub
        {
            [MarshalAs(UnmanagedType.LPStr, SizeConst = 4)]
            public string LPStr;
        }

        private struct LPTStrStructStub
        {
            [MarshalAs(UnmanagedType.LPTStr, SizeConst = 4)]
            public string LPTStr;
        }

        private struct LPWStrStructStub
        {
            [MarshalAs(UnmanagedType.LPWStr, SizeConst = 4)]
            public string LPWStr;
        }

        private struct LPUTF8StrStructStub
        {
            [MarshalAs(UnmanagedType.LPUTF8Str, SizeConst = 4)]
            public string LPUTF8Str;
        }

        private struct BStrStructStub
        {
            [MarshalAs(UnmanagedType.BStr, SizeConst = 4)]
            public string BStr;
        }

        private struct ByValStrStructStub
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 9)]
            public string ByValTStr;
        }

        private struct HStringStructStub
        {
            [MarshalAs(UnmanagedType.HString, SizeConst = 8)]
            public string HString;
        }

        private struct ByValArrayStructStub
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public string ByValArray;
        }

        private struct LPArrayStructStub
        {
            [MarshalAs(UnmanagedType.LPArray, SizeConst = 4)]
            public string LPArray;
        }

        private struct ByValTStrTruncatedStructStub
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
            public string ByValTStrTruncated;
        }
    }
}