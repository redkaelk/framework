using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Runtime.InteropServices;

namespace Redkaelk.Serialization.UnitTests
{
    [TestClass]
    public partial class MarshalStructSerializerTests
    {
        private static readonly MarshalStructSerializer serializer = new MarshalStructSerializer();

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Deserialize_NullBytes()
        {
            serializer.Deserialize<int>(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_Deserialize_EmptyArray()
        {
            serializer.Deserialize<int>(Array.Empty<byte>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_Deserialize_ArrayTooSmall()
        {
            serializer.Deserialize<int>(new byte[3]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_Deserialize_ArrayTooBig()
        {
            serializer.Deserialize<int>(new byte[5]);
        }

        [TestMethod]
        public void Test_SerializeDeserialize_Int32()
        {
            var expected = 42;
            var bytes = serializer.Serialize(expected);
            var actual = serializer.Deserialize<int>(bytes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_SerializeDeserialize_Single()
        {
            var expected = 42.42f;
            var bytes = serializer.Serialize(expected);
            var actual = serializer.Deserialize<float>(bytes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_SerializeDeserialize_Byte()
        {
            byte expected = 42;
            var bytes = serializer.Serialize(expected);
            var actual = serializer.Deserialize<byte>(bytes);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_SerializeDeserialize_DateTime()
        {
            var expected = DateTime.Now;
            serializer.Serialize(expected);
        }

        /// <remarks>
        /// Considerations have been made for making <see cref="Marshal.SizeOf{T}()" />
        /// support enums: https://github.com/dotnet/runtime/issues/12258#issuecomment-473001308
        /// </remarks>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_SerializeDeserialize_Enum()
        {
            var expected = StringSplitOptions.RemoveEmptyEntries;
            serializer.Serialize(expected);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_SerializeDeserialize_CustomStructWithHString()
        {
            var expected = new HStringStructStub()
            {
                HString = "TESTTEST",
            };
            serializer.Serialize(expected);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_SerializeDeserialize_CustomStructWithByValArray()
        {
            var expected = new ByValArrayStructStub()
            {
                ByValArray = "TESTTEST",
            };
            serializer.Serialize(expected);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_SerializeDeserialize_CustomStructWithLPArray()
        {
            var expected = new LPArrayStructStub()
            {
                LPArray = "TESTTEST",
            };
            serializer.Serialize(expected);
        }

        [TestMethod]
        public void Test_SerializeDeserialize_CustomStruct()
        {
            var expected = new StructStub()
            {
                Byte = 1,
                DateTime = new DateTime(2000, 01, 02, 03, 04, 05),
                Enum = StringSplitOptions.RemoveEmptyEntries,
                Int = 2,
            };
            var bytes = serializer.Serialize(expected);
            var actual = serializer.Deserialize<StructStub>(bytes);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Serialize_LPStr()
        {
            var expected = new LPStrStructStub()
            {
                LPStr = "LPStrLPStrLPStrLPStrLPStr",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(8, bytes.Length); // serializes memory address, not content
        }

        [TestMethod]
        public void Test_Serialize_LPTStr()
        {
            var expected = new LPTStrStructStub()
            {
                LPTStr = "LPTStrLPTStrLPTStrLPTStrLPTStr",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(8, bytes.Length); // serializes memory address, not content
        }

        [TestMethod]
        public void Test_Serialize_LPWStr()
        {
            var expected = new LPWStrStructStub()
            {
                LPWStr = "LPWStrLPWStrLPWStrLPWStrLPWStr",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(8, bytes.Length); // serializes memory address, not content
        }

        [TestMethod]
        public void Test_Serialize_LPUTF8Str()
        {
            var expected = new LPUTF8StrStructStub()
            {
                LPUTF8Str = "LPUTF8StrLPUTF8StrLPUTF8StrLPUTF8StrLPUTF8Str",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(8, bytes.Length); // serializes memory address, not content
        }

        [TestMethod]
        public void Test_Serialize_BStr()
        {
            var expected = new BStrStructStub()
            {
                BStr = "BStrBStrBStrBStrBStr",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(8, bytes.Length); // serializes memory address, not content
        }

        [TestMethod]
        public void Test_Serialize_ByValStr()
        {
            var expected = new ByValStrStructStub()
            {
                ByValTStr = "ByVal",
            };
            var bytes = serializer.Serialize(expected);
            Assert.AreEqual(9, bytes.Length);
        }

        [TestMethod]
        public void Test_SerializeDeserialize_ByValTStrTruncatedStruct()
        {
            var expected = new ByValTStrTruncatedStructStub()
            {
                ByValTStrTruncated = "ByValTSt",
            };
            var bytes = serializer.Serialize(expected);
            var actual = serializer.Deserialize<ByValTStrTruncatedStructStub>(bytes);

            Assert.AreEqual("ByValTS", actual.ByValTStrTruncated);
        }
    }
}