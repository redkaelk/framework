using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Linq;

namespace Redkaelk.UnitTests
{
    public partial class EnumerableExtensionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Select_SourceIsNull()
        {
            EnumerableExtensions.Select(null, value => value).ToArray();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Select_SelectorIsNull()
        {
            var source = new int[] { 1, 2, 3 };
            Func<object, object> selector = null;
            EnumerableExtensions.Select(source, selector).ToArray();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_Select_SourceAndSelectorAreNull()
        {
            Func<object, object> selector = null;
            EnumerableExtensions.Select(null, selector).ToArray();
        }

        [TestMethod]
        public void Test_Select_Success()
        {
            var source = new DateTime[]
            {
                new DateTime(2000, 01, 01, 12, 00, 00, 00),
                new DateTime(2001, 01, 01, 12, 00, 00, 00),
            };
            var actual = EnumerableExtensions.Select(source, value => ((DateTime)value).Year);
            Assert.IsNotNull(actual);

            var years = actual.ToArray();
            Assert.AreEqual(2, years.Length);
            Assert.AreEqual(2000, years[0]);
            Assert.AreEqual(2001, years[1]);
        }
    }
}
