﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redkaelk.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_RandomElement_EmptyStringIEnumerable()
        {
            var random = new Random();
            IEnumerable<string> enumerable = Array.Empty<string>();
            var element = random.RandomElement(enumerable);
            Assert.IsNull(element);
        }

        [TestMethod]
        public void Test_RandomElement_EmptyLongIEnumerable()
        {
            var random = new Random();
            IEnumerable<long> enumerable = Array.Empty<long>();
            var element = random.RandomElement(enumerable);
            Assert.AreEqual(0L, element);
        }

        [TestMethod]
        public void Test_RandomElement_StringIEnumerable()
        {
            var random = new Random();
            IEnumerable<string> enumerable = new string[]
            {
                "bird",
                "dog",
                "cat",
            };
            var element = random.RandomElement(enumerable);
            var found = enumerable.Contains(element);
            Assert.IsTrue(found);
        }

        [TestMethod]
        public void Test_RandomElement_LongIEnumerable()
        {
            var random = new Random();
            IEnumerable<long> enumerable = new long[]
            {
                1L,
                2L,
                3L,
            };
            var element = random.RandomElement(enumerable);
            var found = enumerable.Contains(element);
            Assert.IsTrue(found);
        }
    }
}
