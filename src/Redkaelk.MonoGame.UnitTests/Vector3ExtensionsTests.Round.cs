﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_Round_ComponentsAreRounded()
        {
            var tests = new[]
            {
                new { Input = new Vector3(0.0f, 0.0f, 0.0f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(0.1f, 0.0f, 0.0f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(0.0f, 0.1f, 0.0f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(0.0f, 0.0f, 0.1f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(-0.1f, 0.0f, 0.0f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(0.0f, -0.1f, 0.0f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
                new { Input = new Vector3(0.0f, 0.0f, -0.1f), Expected = new Vector3(0.0f, 0.0f, 0.0f) },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var actual = Vector3Extensions.Round(test.Input);
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed.");
            }
        }

        [TestMethod]
        public void Test_Round_LengthMightChange()
        {
            var tests = new[]
            {
                new { Input = new Vector3(0.0f, 0.0f, 0.0f), Expected = 0.0f },
                new { Input = new Vector3(0.1f, 0.0f, 0.0f), Expected = 0.0f },
                new { Input = new Vector3(-0.1f, 0.0f, 0.0f), Expected = 0.0f },
                new { Input = new Vector3(1.0f, 1.0f, 1.0f), Expected = 1.7320508f },
                new { Input = new Vector3(0.9f, 0.9f, 0.9f), Expected = 1.7320508f },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var rounded = Vector3Extensions.Round(test.Input);
                var actual = rounded.Length();
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed.");
            }
        }
    }
}
