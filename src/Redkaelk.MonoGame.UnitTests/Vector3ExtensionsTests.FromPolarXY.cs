﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_FromPolarXY_ConvertsPolarCoordinatesToVectors()
        {
            var tests = new[]
            {
                new { Angle = 0.0f, Magnitude = 1.0f, Expected = new Vector3(1.0f, 0.0f, 0.0f) },
                new { Angle = MathHelper.Pi, Magnitude = 1.0f, Expected = new Vector3(-1.0f, -8.742278E-08f, 0.0f) },
                new { Angle = MathHelper.PiOver2, Magnitude = 1.0f, Expected = new Vector3(-4.371139E-08f, 1.0f, 0.0f) },
                new { Angle = -MathHelper.PiOver2, Magnitude = 1.0f, Expected = new Vector3(-4.371139E-08f, -1.0f, 0.0f) },
                new { Angle = MathHelper.PiOver4, Magnitude = 1.0f, Expected = new Vector3(0.70710677f, 0.70710677f, 0.0f) },
                new { Angle = -MathHelper.PiOver4, Magnitude = 1.0f, Expected = new Vector3(0.70710677f, -0.70710677f, 0.0f) },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var actual = Vector3Extensions.FromPolarXY(test.Angle, test.Magnitude);
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed.");
            }
        }
    }
}
