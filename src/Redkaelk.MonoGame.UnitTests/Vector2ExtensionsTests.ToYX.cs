using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector2ExtensionsTests
    {
        [TestMethod]
        public void Test_ToYX_Numbers()
        {
            var input = new Vector2(1.23f, 4.56f);
            var actual = input.ToYX();
            Assert.AreEqual(input.X, actual.Y);
            Assert.AreEqual(input.Y, actual.X);
        }

        [TestMethod]
        public void Test_ToYX_NotANumber()
        {
            var input = new Vector2(float.NaN, 4.56f);
            var actual = input.ToYX();
            Assert.AreEqual(input.X, actual.Y);
            Assert.AreEqual(input.Y, actual.X);
        }

        [TestMethod]
        public void Test_ToYX_NegativeInfinity()
        {
            var input = new Vector2(float.NegativeInfinity, 4.56f);
            var actual = input.ToYX();
            Assert.AreEqual(input.X, actual.Y);
            Assert.AreEqual(input.Y, actual.X);
        }

        [TestMethod]
        public void Test_ToYX_PositiveInfinity()
        {
            var input = new Vector2(float.PositiveInfinity, 4.56f);
            var actual = input.ToYX();
            Assert.AreEqual(input.X, actual.Y);
            Assert.AreEqual(input.Y, actual.X);
        }
    }
}