﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_ScaleTo_ScalesTheVector()
        {
            var tests = new[]
            {
                new { Vector = new Vector3(0.0f, 0.0f, 0.0f), NewLength = 1.0f, Expected = float.NaN },
                new { Vector = new Vector3(10.0f, 0.0f, 0.0f), NewLength = 1.0f, Expected = 1.0f },
                new { Vector = new Vector3(0.0f, 10.0f, 0.0f), NewLength = 1.0f, Expected = 1.0f },
                new { Vector = new Vector3(0.0f, 0.0f, 10.0f), NewLength = 1.0f, Expected = 1.0f },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var scaledVector = Vector3Extensions.ScaleTo(test.Vector, test.NewLength);
                var actual = scaledVector.Length();
                if (float.IsNaN(actual) && float.IsNaN(test.Expected))
                {
                    continue;
                }

                Assert.AreEqual(test.NewLength, actual, $"Test case at index {testIndex} failed.");
            }
        }
    }
}
