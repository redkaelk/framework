﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_ToAngleXY_CalculatesTheCorrectAngles()
        {
            var tests = new[]
            {
                new { Input = Vector3.Right, Expected = 0.0f },
                new { Input = Vector3.Up, Expected = MathHelper.PiOver2 },
                new { Input = Vector3.Down, Expected = -MathHelper.PiOver2 },
                new { Input = Vector3.Left, Expected = MathHelper.Pi },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var actual = test.Input.ToAngleXY();
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed");
            }
        }
    }
}
