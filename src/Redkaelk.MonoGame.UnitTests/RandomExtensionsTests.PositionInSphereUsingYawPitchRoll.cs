﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_PositionInSphereUsingYawPitchRoll_WithinMinMaxRadius()
        {
            var random = new Random();

            for (var testIndex = 0; testIndex < 1000; testIndex++)
            {
                var minRadius = random.Next(0.0f, 100.0f);
                var maxRadius = random.Next(minRadius, 100.0f);
                var position = random.PositionInSphereUsingYawPitchRoll(minRadius, maxRadius);
                var length = position.Length();
                Assert.IsTrue(length >= minRadius, $"length ({length}) is less than minimum radius ({minRadius})");
                Assert.IsTrue(length < maxRadius, $"length ({length}) is greater than or equal to maximum radius ({maxRadius})");
            }
        }
    }
}
