﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_PositionInCircleUsingPolar_WithinMinMaxRadius()
        {
            var random = new Random();
            const float threshold = 0.000004f;

            for (var testIndex = 0; testIndex < 1000; testIndex++)
            {
                var minRadius = random.Next(0.0f, 50.0f);
                var maxRadius = random.Next(50.0f, 100.0f);
                var position = random.PositionInCircleUsingPolar(minRadius, maxRadius);
                var length = position.Length();

                // it is not possible to simply ensure that length is less than or equal to minRadius therefore a threshold is utilized here
                // the reason being that float cannot represent every single number and will some times end up rounding down to the nearest
                // representable number which might be lower than the specified minRadius 
                var delta = minRadius - length;
                Assert.IsTrue(delta < threshold, $"length ({length}) is less than minimum radius ({minRadius})");

                Assert.IsTrue(length <= maxRadius, $"length ({length}) is greater than or equal maximum radius ({maxRadius})");
            }
        }
    }
}
