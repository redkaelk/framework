﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector2ExtensionsTests
    {
        [TestMethod]
        public void Test_Average_CalculatesTheAverageToBeZero()
        {
            var source = new Vector2[]
            {
                Vector2.Zero,
                Vector2.Zero,
                Vector2.Zero,
                Vector2.Zero,
            };
            var actual = Vector2Extensions.Average(source, e => e);
            Assert.AreEqual(expected: Vector2.Zero, actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForX()
        {
            var source = new Vector2[]
            {
                new Vector2(1.0f, 0.0f),
                new Vector2(2.0f, 0.0f),
                new Vector2(3.0f, 0.0f),
                new Vector2(4.0f, 0.0f),
            };
            var actual = Vector2Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector2(2.5f, 0.0f), actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForY()
        {
            var source = new Vector2[]
            {
                new Vector2(0.0f, 1.0f),
                new Vector2(0.0f, 2.0f),
                new Vector2(0.0f, 3.0f),
                new Vector2(0.0f, 4.0f),
            };
            var actual = Vector2Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector2(0.0f, 2.5f), actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForXYZ()
        {
            var source = new Vector2[]
            {
                new Vector2(4.0f, 1.0f),
                new Vector2(3.0f, 3.0f),
                new Vector2(2.0f, 2.0f),
                new Vector2(1.0f, 4.0f),
            };
            var actual = Vector2Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector2(2.5f, 2.5f), actual);
        }
    }
}
