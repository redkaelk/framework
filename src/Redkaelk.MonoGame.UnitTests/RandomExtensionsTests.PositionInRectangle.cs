﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_PositionInRectangle_InsideRectangle()
        {
            var random = new Random();

            for (var testIndex = 0; testIndex < 1000; testIndex++)
            {
                var minX = random.Next(0.0f, 49.0f);
                var maxX = random.Next(50.0f, 100.0f);
                var minY = random.Next(0.0f, 49.0f);
                var maxY = random.Next(50.0f, 100.0f);
                var position = random.PositionInRectangle(minX, maxX, minY, maxY);
                Assert.IsTrue(position.X >= minX, $"X ({position.X}) is less than minimum X ({minX})");
                Assert.IsTrue(position.X < maxX, $"X ({position.X}) is greater than or equal to maximum X ({maxX})");
                Assert.IsTrue(position.Y >= minY, $"Y ({position.Y}) is less than minimum Y ({minY})");
                Assert.IsTrue(position.Y < maxY, $"Y ({position.Y}) is greater then or equal to maximum Y ({maxY})");
            }
        }
    }
}
