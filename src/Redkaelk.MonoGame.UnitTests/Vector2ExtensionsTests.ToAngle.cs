﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector2ExtensionsTests
    {
        [TestMethod]
        public void Test_ToAngle_CalculatesTheCorrectAngles()
        {
            var tests = new[]
            {
                new { Input = new Vector2(1.0f, 0.0f), Expected = 0.0f },
                new { Input = new Vector2(0.0f, 1.0f), Expected = MathHelper.PiOver2 },
                new { Input = new Vector2(0.0f, -1.0f), Expected = -MathHelper.PiOver2 },
                new { Input = new Vector2(-1.0f, 0.0f), Expected = MathHelper.Pi },
                new { Input = new Vector2(-1.0f, -0.0f), Expected = -MathHelper.Pi },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var actual = test.Input.ToAngle();
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed");
            }
        }
    }
}
