﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        public void Test_PositionInCube_InsideCube()
        {
            var random = new Random();

            for (var testIndex = 0; testIndex < 1000; testIndex++)
            {
                var minX = random.Next(0.0f, 100.0f);
                var maxX = random.Next(minX, 100.0f);
                var minY = random.Next(0.0f, 100.0f);
                var maxY = random.Next(minY, 100.0f);
                var minZ = random.Next(0.0f, 100.0f);
                var maxZ = random.Next(minZ, 100.0f);
                var position = random.PositionInCube(minX, maxX, minY, maxY, minZ, maxZ);
                Assert.IsTrue(position.X >= minX, $"X ({position.X}) is less than minimum X ({minX})");
                Assert.IsTrue(position.X < maxX, $"X ({position.X}) is greater than or equal to maximum X ({maxX})");
                Assert.IsTrue(position.Y >= minY, $"Y ({position.Y}) is less than minimum Y ({minY})");
                Assert.IsTrue(position.Y < maxY, $"Y ({position.Y}) is greater then or equal to maximum Y ({maxY})");
                Assert.IsTrue(position.Z >= minZ, $"Z ({position.Z}) is less than minimum Z ({minZ})");
                Assert.IsTrue(position.Z < maxZ, $"Z ({position.Z}) is greater then or equal to maximum Z ({maxZ})");
            }
        }
    }
}
