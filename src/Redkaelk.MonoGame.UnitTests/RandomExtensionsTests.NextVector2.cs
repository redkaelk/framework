﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using System;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class RandomExtensionsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NextVector2_ShouldThrowArgumentNullException_WhenRandomIsNull()
        {
            // Arrange
            Random random = null;
            var min = new Vector2(0, 0);
            var max = new Vector2(10, 10);

            // Act
            RandomExtensions.NextVector2(random, min, max);
        }

        [TestMethod]
        public void NextVector2_ShouldReturnVectorWithinRange()
        {
            // Arrange
            var random = new Random();
            var min = new Vector2(0, 0);
            var max = new Vector2(10, 10);

            // Act
            var result = RandomExtensions.NextVector2(random, min, max);

            // Assert
            Assert.IsTrue(result.X >= min.X && result.X <= max.X);
            Assert.IsTrue(result.Y >= min.Y && result.Y <= max.Y);
        }

        [TestMethod]
        public void NextVector2_ShouldReturnDifferentResults()
        {
            // Arrange
            var random = new Random();
            var min = new Vector2(0, 0);
            var max = new Vector2(10, 10);

            // Act
            var result1 = RandomExtensions.NextVector2(random, min, max);
            var result2 = RandomExtensions.NextVector2(random, min, max);

            // Assert
            Assert.AreNotEqual(result1, result2);
        }
    }
}
