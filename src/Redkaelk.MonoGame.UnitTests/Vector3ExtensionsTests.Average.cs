﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_Average_CalculatesTheAverageToBeZero()
        {
            var source = new Vector3[]
            {
                Vector3.Zero,
                Vector3.Zero,
                Vector3.Zero,
                Vector3.Zero,
            };
            var actual = Vector3Extensions.Average(source, e => e);
            Assert.AreEqual(expected: Vector3.Zero, actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForX()
        {
            var source = new Vector3[]
            {
                new Vector3(1.0f, 0.0f, 0.0f),
                new Vector3(2.0f, 0.0f, 0.0f),
                new Vector3(3.0f, 0.0f, 0.0f),
                new Vector3(4.0f, 0.0f, 0.0f),
            };
            var actual = Vector3Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector3(2.5f, 0.0f, 0.0f), actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForY()
        {
            var source = new Vector3[]
            {
                new Vector3(0.0f, 1.0f, 0.0f),
                new Vector3(0.0f, 2.0f, 0.0f),
                new Vector3(0.0f, 3.0f, 0.0f),
                new Vector3(0.0f, 4.0f, 0.0f),
            };
            var actual = Vector3Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector3(0.0f, 2.5f, 0.0f), actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForZ()
        {
            var source = new Vector3[]
            {
                new Vector3(0.0f, 0.0f, 1.0f),
                new Vector3(0.0f, 0.0f, 2.0f),
                new Vector3(0.0f, 0.0f, 3.0f),
                new Vector3(0.0f, 0.0f, 4.0f),
            };
            var actual = Vector3Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector3(0.0f, 0.0f, 2.5f), actual);
        }

        [TestMethod]
        public void Test_Average_CalculatesTheAverageForXYZ()
        {
            var source = new Vector3[]
            {
                new Vector3(4.0f, 1.0f, 1.0f),
                new Vector3(3.0f, 3.0f, 2.0f),
                new Vector3(2.0f, 2.0f, 3.0f),
                new Vector3(1.0f, 4.0f, 4.0f),
            };
            var actual = Vector3Extensions.Average(source, e => e);
            Assert.AreEqual(expected: new Vector3(2.5f, 2.5f, 2.5f), actual);
        }
    }
}
