﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Redkaelk.MonoGame.UnitTests
{
    public partial class Vector3ExtensionsTests
    {
        [TestMethod]
        public void Test_AngleXZ_AngleIsCalculated()
        {
            var left = Vector3.Left;
            var right = Vector3.Right;
            var forward = Vector3.Forward;
            var backward = Vector3.Backward;

            var tests = new[]
            {
                new { From = right, To = right, Expected = 0.0f },
                new { From = right, To = backward, Expected = -MathHelper.PiOver2 },
                new { From = right, To = left, Expected = -MathHelper.Pi },
                new { From = right, To = forward, Expected = MathHelper.PiOver2 },

                new { From = backward, To = backward, Expected = 0.0f },
                new { From = backward, To = left, Expected = -MathHelper.PiOver2 },
                new { From = backward, To = forward, Expected = MathHelper.Pi },
                new { From = backward, To = right, Expected = MathHelper.PiOver2 },

                new { From = left, To = left, Expected = 0.0f },
                new { From = left, To = forward, Expected = -MathHelper.PiOver2 },
                new { From = left, To = right, Expected = MathHelper.Pi },
                new { From = left, To = backward, Expected = MathHelper.PiOver2 },

                new { From = forward, To = forward, Expected = 0.0f },
                new { From = forward, To = right, Expected = -MathHelper.PiOver2 },
                new { From = forward, To = backward, Expected = -MathHelper.Pi },
                new { From = forward, To = left, Expected = MathHelper.PiOver2 },
            };

            for (var testIndex = 0; testIndex < tests.Length; testIndex++)
            {
                var test = tests[testIndex];
                var actual = Vector3Extensions.AngleXZ(test.From, test.To);
                Assert.AreEqual(test.Expected, actual, $"Test case at index {testIndex} failed.");
            }
        }
    }
}
