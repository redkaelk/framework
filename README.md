![Redkaelk Logo](./src/Icon.png)

# Redkaelk

Welcome to the Redkaelk framework, an open-source project designed to ease your MonoGame development process. This guide provides information on how to install and download the framework using either NuGet or Git submodules.

## Table of Contents
- [Introduction](#introduction)
- [Installation](#installation)
  - [NuGet](#nuget)
  - [Git Submodules](#git-submodules)

## Introduction

The Redkaelk framework is a modular framework designed for ease of use and flexibility. While some of the modules are not strictly for MonoGame, other can be used with other .NET game frameworks or even in non-game development.

## Installation

You can install the Redkaelk framework using either NuGet or Git submodules, depending on your preference and project requirements. NuGet is the most natural way to add packages in .NET and Git submdules makes it easier for you to step through Redkaelk source code when debugging and make modifications.

### NuGet

The Redkaelk framework is devided into multiple Nuget packages.

| Package | |
|-|-|
| [Redkaelk](https://www.nuget.org/packages/Redkaelk) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk) |
| [Redkaelk.EntityComponentSystem](https://www.nuget.org/packages/Redkaelk.EntityComponentSystem) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk.EntityComponentSystem) |
| [Redkaelk.EntityComponentSystem.MonoGame.DesktopGL](https://www.nuget.org/packages/Redkaelk.EntityComponentSystem.MonoGame.DesktopGL) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk.EntityComponentSystem.MonoGame.DesktopGL) |
| [Redkaelk.EntityComponentSystem.MonoGame.WindowsDX](https://www.nuget.org/packages/Redkaelk.EntityComponentSystem.MonoGame.WindowsDX) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk.EntityComponentSystem.MonoGame.WindowsDX) |
| [Redkaelk.MonoGame.DesktopGL](https://www.nuget.org/packages/Redkaelk.MonoGame.DesktopGL) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk.MonoGame.DesktopGL) |
| [Redkaelk.MonoGame.WindowsDX](https://www.nuget.org/packages/Redkaelk.MonoGame.WindowsDX) | ![NuGet Downloads](https://img.shields.io/nuget/dt/Redkaelk.MonoGame.WindowsDX) |

To install the Redkaelk Framework via NuGet, follow these steps:

1. **Open your project in Visual Studio**.
2. **Right-click on the project in the Solution Explorer** and select **Manage NuGet Packages**.
3. **Search for the Redkaelk Framework** packages.
4. **Install the desired packages**.

Alternatively, you can use the .NET CLI to install the packages:

```sh
dotnet add package Redkaelk
dotnet add package Redkaelk.EntityComponentSystem
dotnet add package Redkaelk.EntityComponentSystem.MonoGame.DesktopGL
dotnet add package Redkaelk.EntityComponentSystem.MonoGame.DesktopDX
dotnet add package Redkaelk.MonoGame.DesktopGL
dotnet add package Redkaelk.MonoGame.DesktopDX
```

### Git Submodules

NB! Using submodules is for the more advanced programmer, in terms of Git. Besides the steps mentioned below you might also want to consider forking the Redkaelk repositories you chose to use.

To include the Redkaelk Framework as Git submodules, follow these steps:

1. In the termina1, navigate to where in your project you want the submodules
2. Add the desired submodules:
   1. SSH

    ```sh
    git submodule add git@gitlab.com:redkaelk/redkaelk.shared.git Redkaelk.Shared
    git submodule add git@gitlab.com:redkaelk/redkaelk.monogame.shared.git Redkaelk.MonoGame.Shared
    git submodule add git@gitlab.com:redkaelk/redkaelk.entitycomponentsystem.shared.git Redkaelk.EntityComponentSystem.Shared
    git submodule add git@gitlab.com:redkaelk/redkaelk.entitycomponentsystem.monogame.shared.git Redkaelk.EntityComponentSystem.MonoGame.Shared
    ```
   2. HTTP
    ```sh
    git submodule add https://gitlab.com/redkaelk/redkaelk.shared.git Redkaelk.Shared
    git submodule add https://gitlab.com/redkaelk/redkaelk.monogame.shared.git Redkaelk.MonoGame.Shared
    git submodule add https://gitlab.com/redkaelk/redkaelk.entitycomponentsystem.shared.git Redkaelk.EntityComponentSystem.Shared
    git submodule add https://gitlab.com/redkaelk/redkaelk.entitycomponentsystem.monogame.shared.git Redkaelk.EntityComponentSystem.MonoGame.Shared
    ```

3. Initialize and update the submodules:

```sh
git submodule update --init --recursive
```
